# List API

---

Une API permettant de créer un liste

---

## Table of Contents

1. [BDD Info](#bdd-info)
2. [Back Info](#back-info)
3. [Front Info](#front-info)

## BDD Info

---

- Executer le fichier docker compose afin de lancer la base de donnée MongoDB
- Vous avez egalement acces à l'interface Mongo-express à l'adresse : http://localhost:8081/

## Back Info

---

ajouter le fichier .env avec les informations suivantes :

```
DB_URL="mongodb://root:example@localhost:27017"
PORT=4000
```

Les 4 routes sont :

- GET (/list) Recupère la liste
- POST (/list) Enregistre un nouvel élément dans la liste
- DELETE (/list/:id) Supprime un élément de la liste
- PATCH (/list) Modifie un élément de la liste

### WiP

- Ajout de l'authentification (JWT, Bcrypt)
- Ajout de Swagger/OpenAPI pour la documentation.

## Front Info

---

### WiP

- Ajout de la page d'authentification
- Ajout de la modification
