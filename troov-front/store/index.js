export const state = () => ({
  tasks: [],
});

export const mutations = {
  INIT_TASK(state, task) {
    state.tasks = [{ content: task, done: false }, ...state.tasks];
  },
  ADD_TASK(state, task) {
    state.tasks = [{ content: task, done: false }, ...state.tasks];
  },
  REMOVE_TASK(state, task) {
    state.tasks.splice(state.tasks.indexOf(task), 1);
  },
};
