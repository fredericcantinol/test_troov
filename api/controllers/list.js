const List = require('../models/List')
const mongodb = require('mongodb')

const getList = async (req, res) => {
    try {
        const list = await List.find()
        res.status(200)
        res.send(list)
    } catch (error) {
        res.status(404)
        res.send({ error: 'Cannot get the list' })
    }
}

const addElement = async (req, res) => {
    try {
        const list = new List({
            title: req.body.title,
        })
        await list.save()
        res.status(200)
        res.send(list)
    } catch (error) {
        res.status(404)
        res.send({ error: 'Cannot add element to the list' })
    }
}
const patchElement = async (req, res) => {
    const filter = new mongodb.ObjectId(req.body._id.toString())
    const update = req.body.title
    try {
        await List.updateOne(
            { _id: filter },
            {
                title: update,
            }
        )
        res.status(200)
        res.send()
    } catch (error) {
        res.status(404)
        res.send({ error: 'Cannot patch element' })
    }
}

const deleteElement = async (req, res) => {
    try {
        await List.deleteOne({
            _id: new mongodb.ObjectId(req.params.id.toString()),
        })
        res.status(204).send()
    } catch {
        res.status(404)
        res.send({ error: "Element doesn't exist!" })
    }
}

module.exports = { getList, addElement, patchElement, deleteElement }
