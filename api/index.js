const express = require('express')
const mongoose = require('mongoose')
const routes = require('./routes')
const cors = require('cors')
const bodyParser = require('body-parser')

require('dotenv').config()

//.env variables
const dbUrl = process.env.DB_URL
const port = process.env.PORT

// Connect to MongoDB database
mongoose.connect(dbUrl, { useNewUrlParser: true }).then(() => {
    const app = express()
    app.use(cors())
    app.use(
        bodyParser.urlencoded({
            extended: true,
        })
    )
    app.use(bodyParser.json())
    app.use(express.json())

    app.use('/api', routes)

    app.listen(port, () => {
        console.log('Server has started!')
    })
})
