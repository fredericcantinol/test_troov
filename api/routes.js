const express = require('express')
const router = express.Router()
const {
    getList,
    addElement,
    deleteElement,
    patchElement,
} = require('./controllers/list')

// Get all items of list
router.get('/list', async (req, res) => getList(req, res))

// Post new item in list
router.post('/list', async (req, res) => addElement(req, res))

// Post new item in list
router.patch('/list', async (req, res) => patchElement(req, res))

//Delete one item of list
router.delete('/list/:id', async (req, res) => deleteElement(req, res))

module.exports = router
